FROM python:3-alpine

ADD requirements.txt /nyandroid/

WORKDIR /nyandroid/
VOLUME /nyandroid/code/data/

RUN apk add --virtual .build-deps gcc musl-dev libffi-dev openssl-dev \
 && pip install -r requirements.txt \
 && apk del --purge .build-deps

ADD code /nyandroid/code
ADD images /nyandroid/images

ENV NYANDROID_TOKEN=""

CMD python code/bot.py